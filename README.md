docker-gcc
============

[![pipeline status](https://gitlab.com/mmoelle1/docker-gcc/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/docker-gcc/commits/master)

Repository contains a customizable `Dockerfile` based on ubuntu:16.04, 18.04,
and 20.04 with **GNU C/C++ compiler environment**. The generated docker images
moreover contain the tools `cmake`, `doxygen`, `git`, `ninja`, `svn`, `wget`
and the libraries `arrayfire`, `blas`, `boost`, and `lapack`. All containers
contain the Intel OpenCL driver for Intel CPUs.

Available docker images:

**Ubuntu 16.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-gcc:4.9ubuntu16.04](registry.gitlab.com/mmoelle1/docker-gcc:4.9ubuntu16.04)
- [registry.gitlab.com/mmoelle1/docker-gcc:5ubuntu16.04](registry.gitlab.com/mmoelle1/docker-gcc:5ubuntu16.04)

**Ubuntu 18.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-gcc:6ubuntu18.04](registry.gitlab.com/mmoelle1/docker-gcc:6ubuntu18.04)
- [registry.gitlab.com/mmoelle1/docker-gcc:7ubuntu18.04](registry.gitlab.com/mmoelle1/docker-gcc:7ubuntu18.04)
- [registry.gitlab.com/mmoelle1/docker-gcc:8ubuntu18.04](registry.gitlab.com/mmoelle1/docker-gcc:8ubuntu18.04)

**Ubuntu 20.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-gcc:8ubuntu20.04](registry.gitlab.com/mmoelle1/docker-gcc:8ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-gcc:9ubuntu20.04](registry.gitlab.com/mmoelle1/docker-gcc:9ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-gcc:10ubuntu20.04](registry.gitlab.com/mmoelle1/docker-gcc:10ubuntu20.04)

